# DNS Lerndokumentation, Edin Ajdarpasic


## 09.01.2024

### Auftrag: DNS Server aufsetzten

Heute bekamen wir Zeit, um einen DNS Server aufzusetzen. Entweder auf Windows oder falls man es wagt, auf Linux. Zurückblickend zum M117 hatten wir schon zwei VM's aufgesetzt, welche optimal für dieses Projekt verwendet werden können, um Klienten zu simulieren.

#### OS-Installation

zuerst lud ich diese Serverversion von Windows herunter.

![Installationsbild1](Screenshot%202024-01-09%20144058.png)

---
#### DNS-Installation

Als ich das Betriebssystem dann endlich installierte, wandte ich mich an die eigentliche Arbeit, und zwar den DNS Server so richtig aufzusetzen und zu konfigurieren.

Also klickte ich auf das Feld *Rollen und Features* um die Serverrolle zu installieren. Ich klickte mich hindurch und installierte das: (DNS-Server)

![Serverbild1](Screenshot%202024-01-10%20212921.png)

---
#### DNS-Konfiguration

Danach kreirte ich zwei Zonen, und zwar:
* Die Forward-Lookup-Zone
* Die Reverse-Lookup-Zone

Diese Zonen sind aus guten Gründen existent. Da in einem DNS-Server Reverse-Lookup-Zone verwendet wird um IP-Adressen in Hostnamen umzuwandeln, wobei im Gegensatz dazu die Forward-Lookup-Zone verwendet wird, um genau das Gegenteil zu erreichen. Die Umsetzung von Hostnamen zu IP-Adressen.

Das sieht dann so aus:

![Serverbild2](Screenshot%202024-01-10%20214433.png)

![Serverbild3](Screenshot%202024-01-10%20214506.png)

---
#### Erster Kontakt zwischen Clienten und Servern

Ich versuchte dann mit dem zuvor erstellten PC-02 vom M117 den DNS-Server zu pingen, ging aber nicht. Dann versuchte ich es über die NAT vergebene IP, was dann funktionierte. Ich fand dann heraus, dass der DNS-Server im *Ethernet1* eine ungültige IP zugewiesen bekommen hat. Also änderte ich das geschwind und bemerkte, dass es die Option gab, die IP-Adresse für den Primären DNS-Server einzugeben. Dann dachte ich mir, dass ist ja schon der DNS-Server und liess es leer. Dann bekam ich die Benachrichtigung, dass das DNS-Serversystem auf diesem Gerät erkannt wurde und ich dann automatisch die DNS funktion auf diesem Server in diesem Netzwerk freigeschaltet habe. Ich schrieb dann auch auf dem PC-02 im *Ethernet1* die IP-Adresse des genannten Servers ins Feld

Dies sah dann wie folgt aus:

![Serverbild4](Screenshot%202024-01-10%20220916.png)

---
#### Host-Pinging

Dann testete ich es und es funktionierte Einwandfrei.

![Serverbild5](image-2.png)

---
#### \\> nslookup (Versuch 1)

Dann versuchte ich auch denn command: */> nslookup*

Es steht für "Namensserver-Suche" /"**N**ame-**S**erver-**Lookup**"

Zuerst stellte ich fest, dass es den DNS-Server nicht erreichte, da der PC-02 zwei Netzwerkadapter hat und die ganze Zeit über nur im NAT statt in meinem erstellten Netzwerkadapter nach diesem DNS-Server sucht. Dazu fand ich jedoch eine Lösung. Ich gab ein:
*/> nslookup sotagmbh.com 192.168.100.2*

Schlussendlich sah die Antwort dann so aus:


![Serverbild6](Screenshot%202024-01-10%20223853.png)

Das war die einzige andere valide Option, den Server durch nslookup zu finden. aber wie man schlussendlich sehen kann, habe ich mit etwas grübeln im Web gefunden was ich sonst noch tun kann. Ich habe mich von DNS-Servern assistieren lassen, um einen DNS-Server aufzusetzen. Wie ironisch...

An dem Punkt muss ich jedoch sagen, dass ich mich mit diesem Command immernoch nicht zufrieden gebe und ich dann später darauf zurückgreifen werde.

---
#### Webseiten Pingen

Ich wurde jetzt ein bisschen übermutig, da alles soweit funktionierte und versuchte die Webseite "20min.ch" zu pingen. Zu all meiner Freude funktionierte es tatsächlich!

![Serverbild7](Screenshot%202024-01-10%20230238.png)

---
Aus reiner Freude an meinen Erreich pingte ich noch weitere Seiten an.

![Serverbild8](Screenshot%202024-01-10%20230853.png)

---
Und jetzt pingte ich den DNS-Server unter dem Namen allein:

![Serverbild9](Screenshot%202024-01-10%20231552.png)

Jetzt kann man erkennen, dass der Computer ja den Namen/Adresse pingt, wenn man aber rechts vom Namen schaut, sieht man die IP-Adresse! Somit konnte ich jegliche Probleme lösen und habe es geschafft!

Hier unten noch nachgestellt:

---
C:\Users\KEHURS\\>ping Server.sotagmbh.com

Pinging Server.sotagmbh.com **[192.168.100.2]** with 32 bytes of data...

---
#### Lokale Zuordnung von Hosts (Name & IP)

Und hier auf dem  Bild kann man auch erkennen, dass der DNS-Server, lokal Gerätenamen und IP-Adressen zuordnen kann. Also könnte ich entweder die IP-Adresse dieses Clients reinschreiben oder den Gerätenamen und es wird auf einer oder der anderen Art genau diesen Client zeigen.

Hier als Grafik, mit IP- und Namenssuche:

![Serverbild10](Screenshot%202024-01-14%20183203.png)

![Serverbild10](Screenshot%202024-01-14%20182459.png)

In diesen Bildern habe ich jedoch den NAT-Netzwerkadapter für den PC-02 deaktiviert, sodass er den Command "/> nslookup" durchführen kann und nicht im falschen Netz sucht.

---
#### \\> nslookup (Versuch 2)

Auch von dem habe ich ein Bild gemacht, wobei ich mich wundere, warum es ihn nicht als Default Server, jedoch mit IP-Adresse erkennt.

![Serverbild11](Screenshot%202024-01-14%20192826.png)

Dann habe ich aber ein bisschen rumgegrübelt und habe "\\> nslookup 192.168.100.2" geschrieben in der hoffnung, dass der Client den Server erkennt, was tatsächlich der Fall war. Dann habe ich nochmals " \\> nslookup" geschrieben und dann hat es ihn erkannt. Ich nehme an, dass es eine Art *Discovery* geschickt hat wie beim Thema DHCP. 

![Serverbild12](Screenshot%202024-01-14%20192858.png)

Also hat er den DNS-Server dich noch als *Default Server* annerkennen können und habe somit auch das Problem mit dem Command "\\> nslookup" lösen können.

---

#### Reflexion

Die Installation des DNS-Servers war eine spannende und lehrreiche Erfahrung. Es gab ständig neue Erkenntnisse und Anpassungen, von der OS-Installation bis zur Konfiguration der Forward- und Reverse-Lookup-Zonen. Durch eigenes Grübeln und Experimentieren wurden die anfänglichen Schwierigkeiten bei der Netzwerkkonfiguration und dem Umgang mit Befehlen wie "nslookup" gelöst. Das Gefühl der Selbstbewältigung wurde durch den Erfolg beim Pingen von Webseiten und dem Testen des DNS-Servers selbst verstärkt. Die Ironie, sich von DNS-Servern im Web unterstützen zu lassen, um letztendlich einen DNS-Server aufzusetzen, machte das Projekt faszinierend. Insgesamt war es ein spannendes Erlebnis, welches mir geholfen hat, meine technischen Fähigkeiten zu verbessern und ein tieferes Verständnis für Netzwerkdienste zu entwickeln. 


## 16.01.2024

### Auftrag Samba installation auf Ubuntu 22.04.3

Heute bekamen wir den Auftrag, Samba auf Ubuntu per Kommandozeile zu installieren. Zuersts war ich verwirrt aber ich habe mich dann an die Arbeit gesetzt.

Somit lud ich das Ubuntu Desktop 22.04.3 Betriebssystem herunter und erstellte eine VM damit. Dies wäre die unten eingeblendete Version.

![Installationsbild1](Screenshot%202024-01-16%20152342.png)

---
Dann konfiguerierte ich die VM entsprechend mit einem "custom network adapter" womit ich dann diese WM in das gleiche Netz, wie die Windows VM einfügte.

Dann wandte ich mich zum eigentlichen Arbeitsauftrag. Also gab ich mir zuerst eine IP-Adresse. Und zwar 192.168.100.10

![Ubuntubild1](Screenshot%202024-01-21%20185213.png)

---
Danach ging es dann zur Kommandozeile, wo ich dann eine Liste von Codes, die ich nach Reihenfolge reinschreiben musste.

- sudo apt update

- sudo apt install samba

- systemctl status smbd --no-pager -l

- sudo systemctl enable --now smbd

- sudo ufw allow samba

- sudo usermod -aG sambashare $USER

- sudo smbpasswd -a $USER

---
Dann suchte ich mir einen Ordner aus, wo ich dieses Samba-Experiment durchlaufen möchte. Ich entschied mich für den Pictures Ordner, wo ich dann noch einen Ordner drinnen einfügte.

Das sieht dann so aus:

![Ubuntubild2](Screenshot%202024-01-21%20185646.png)

Dazu muss ich den "Local Network Share" Dienst aktivieren.

![Ubuntubild3](Screenshot%202024-01-21%20190444.png)

---
An diesem Punkt hat man dann den Grossteil der Arbeit dann schon hinter sich und man kann dan mit der manuellen Ordnersuche fortfahren.

Somit gibt man also beim Feld "Other Locations" ein:

- smb://ip-addresse/ordnername

Somit währe es in meinem Fall:

- smb://192.168.100.10/pictures 

Und dann sollte man zu seinem Ordner weitergeleitet werden, wenn man sich mit dem Passwort authetnifizieren kann. Das sieht dann so aus:

![Ubuntubild](Screenshot%202024-01-21%20191302.png)

---
Und wenn man alles richtig gemacht hat, soll man zu dem dedizierten Ordner weitergeleitet werden.

In der Windows VM, im Explorer kann man den Ordner "Network" öffnen und dann in diesem folgendes reinschreiben:

- \\\ip-adresse\ordnername

Und in meinem Fall währe das:

- \\\192.168.100.10\Pictures

Wonach dann der Ordner von der Ubuntu-VM auf der Windows VM geöffnet wird!

Dann sieht das so aus:

![Ubuntubild5](Screenshot%202024-01-21%20192814.png)

![Ubuntubild6](Screenshot%202024-01-21%20193557.png)

Und so einfach kann man über mehrere diverse Computer auf einen Ordner zugreifen!