# DHCP Lerndokumentation von Edin Ajdarpasic


## DHCP IP-Adressvergabe bei den Clients aktivieren

Um die IP-Adressen via DHCP zu beziehen, wählte ich diese aus und füllte das "DHCP zur Konfiguration verwenden" Feld aus.


## Konfiguration eines DHCP-Servers:
Nachdem ich den DHCP-Server ausgewählt hatte, klickte ich auf den Knopf unten rechts und wählte „DHCP-Server einrichten“. Daraufhin erschien folgendes Pop-up: Ich habe die Adressuntergrenze und Adressobergrenze (192.168.0.150 - 192.168.0.170 ) überprüft und festgestellt, dass die IP-Range bereits korrekt ist.
Dann wählte ich den zweiten Router und erhielt folgendes:
Ich gab die gewünschte IP-Adresse und MAC-Adresse des Client 3 an. Danach klickte ich auf "Hinzufügen".

## Reaktion folgt nach Aktion:
Dann wollte ich überprüfen, ob alles funktioniert. Ich schaute den Datenaustausch des Servers an. In dem Bild ist die IP-vergabe mit DHCP zu sehen. Da ich nicht verstanden habe, was bei den grünen Einträgen passiert, schaute ich unten nach:
Ich verstand dadurch, dass der Client hier überprüft, ob die IP-Adressen bereits eine MAC-Adresse zugewiesen haben, bevor sie diese IP-Adresse requesten.

![Aktionsmodus](Aktionsmodus.png)


## IP-Adresse von Client 3 überprüfen:
Ebenfalls wollte ich überprüfen, ob der Client 3 die IP-Adreses besitzt, welche ich diesem im DHCP-Server zugewiesen habe.
Da die IP-Adresse stimmte, war ich zufrieden damit. 



# Laborübung 1 - DHCP mit Cisco Packet Tracer:

## Router-Konfiguration auslesen

### Für welches Subnetz ist der DHCP Server aktiv?
192.168.32.0 - 192.168.32.254

### Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle):

| IP-Adressen   | MAC-Adresse    |
| ------------- | -------------- |
| 192.168.32.34 | 0007.ECB6.4534 |
| 192.168.32.32 | 0050.0F4E.1D82 |
| 192.168.32.31 | 0001.632C.3508 |
| 192.168.32.33 | 00E0.8F4E.65AA |
| 192.168.32.30 | 0009.7CA6.4CE3 |
| 192.168.32.35 | 00D0.BC52.B29B |


### In welchem Range vergibt der DHCP-Server IPv4 Adressen?

Es steht: 
ip dhcp excluded-address 192.168.32.1 192.168.32.29
ip dhcp excluded-address 192.168.32.163 192.168.32.234

Somit kann man daraus ziehen, dass man nur Adressen im Range 192.168.32.30 - 192.168.32.162 benutzen kann. In anderen Worten: Es stehen 132 IP-Adressen zur Verfügung.


### Was hat die Konfiguration ip dhcp excluded-address zur Folge?

Der DHCP-Server kann solche Adressen nicht vergeben, da sie eben im "excluded" Zustand sind. Excluded ist das englische Wort für ausgeschlossen. 

Sprich: 

Steht so etwas: "ip dhcp excluded-address 192.168.32.1 192.168.32.10", sind die Adresse 192.168.32.1 bis und mit 192.168.32.10 nicht verwendbar. Dies kann aus mehreren Gründen sein, wie z.B. dass diese Adressen nur für bestimmte Geräte konfiguriert worden sind.


### Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

Ohne irgendwelchen "excluded-adress-ranges", könnte der DHCP Server soviele Adressen vergeben, wie hoch die CIDR-Notation ist.

Sprich: in einem /24 Netz könnte der Server 254 Adressen vergeben.

Das sieht dann so aus:
[CLI.show-ip-dhcp-pool](Screenshot 2023-12-10 181817.png)


# Auftrag, DHCP-Server aufsetzen

Wir bekamen den Auftrag, einen DHCP-Server aufzusetzen, welcher mit dem Ubuntu-Betriebssystem agierte. Somit erstellte ich eine neue Virtual-Machine und gab der VM einen zweiten Netzwerkadapter, nebst dem NAT. Dann installierte ich die Ubuntu-DHCP-Server ISO file (Version 22.04.3).

![Installationsbild1](Installationsbild1.png)


Ubuntu schlägt vor, dass ich Updates installiere und fragt mich auch danach. Ich nahm an und der Prozess begann.

![Installationsbild2](Screenshot 2023-12-18 200415.png)


Dann kam die Frage auf, was für eine Installationsart, zudem ich Standard wählte.

![Installationsbild3](Screenshot 2023-12-18 201050.png)


Der wichtige Teil ist jetzt, die IP und Subnetzmaske zu konfigurieren.

![Installationsbild3](Screenshot 2023-12-18 201824.png)


Somit habe ich wie in dem unten verlinkten Bild, eine dynamische NAT-Netzwerkkarte und eine dynamische Netzwerkkarte in der VM.

![Installationsbild3](Screenshot 2023-12-18 202437.png)

Schlussendlich musste ich meine Logindaten einfügen und bestätigen, zudem ich kein Bild eingeblendet habe, da ich es schon gemacht habe, ohne es zu Screenshotten, kann aber zeigen, wie der Login-Prozess aussieht auf meiner VM.

- Nutzer: Edin
- Logindaten: Edin
- Passwort: Wird ich nicht verraten ;)

(Dieses Bild folgt zwar erst, nach der endgültigen Installation, man kann aber sehen, dass ich eingeloggt bin.)
![Installationsbild3](Screenshot 2023-12-18 202749.png)


Und jetzt nach langem Hin und Her, die endgültige Installation

![Installationsbild3](Screenshot 2023-12-18 203209.png)


Jetzt habe ich das "root framework" per Kommandozeile installiert. Ich weiss jetzt aber nicht wie weiter und deswegen warte ich, bis auf den Unterricht im M123 um die nächsten Schritte herauszufinden.